function factorial(n) {
  if (n === 0 || n === 1) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}

console.log("factorial");
console.log(factorial(5)); // 120
console.log(factorial(3)); // 6

// -------

function distance(x1, y1, x2, y2) {
  return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
}

console.log("distance");
console.log(distance(0, 0, 3, 4)); // 5
console.log(distance(0, 0, 10, 10)); // 14.142135623730951

// -------

function isPrime(num) {
  if (num < 2) {
    return false;
  }

  for (let i = 2; i < num; i++) {
    if (num % i === 0) {
      return false;
    }
  }

  return true;
}

console.log("isPrime");
console.log(isPrime(1)); // false
console.log(isPrime(7)); // true
console.log(isPrime(10)); // false

// -------

function binomialCoefficient(n, k) {
  if (k > n) {
    return 0;
  }

  if (k === 0 || k === n) {
    return 1;
  }

  return binomialCoefficient(n - 1, k - 1) + binomialCoefficient(n - 1, k);
}

console.log("binomialCoefficient");
console.log(binomialCoefficient(5, 3)); // 10
console.log(binomialCoefficient(10, 5)); // 252
console.log(binomialCoefficient(10, 10)); // 1

// -------

function hammingDistance(str1, str2) {
  if (str1.length !== str2.length) {
    return NaN;
  }

  let distance = 0;

  for (let i = 0; i < str1.length; i++) {
    if (str1[i].toLowerCase() !== str2[i].toLowerCase()) {
      distance++;
    }
  }

  return distance;
}

console.log("hammingDistance");
console.log(hammingDistance("rover", "river")); // 1
console.log(hammingDistance("rover", "Rover")); // 0
console.log(hammingDistance("mountain", "car")); // NaN

// -------

function leastCommonMultiple(arr) {
  let max = Math.max(...arr);
  let multiple = max;

  while (true) {
    let isLCM = true;
    for (let i = 0; i < arr.length; i++) {
      if (multiple % arr[i] !== 0) {
        isLCM = false;
        break;
      }
    }
    if (isLCM) {
      return multiple;
    }
    multiple += max;
  }
}

console.log("leastCommonMultiple");
console.log(leastCommonMultiple([2, 3])); // 6
console.log(leastCommonMultiple([2, 3, 4])); // 12
console.log(leastCommonMultiple([9, 18])); // 18
